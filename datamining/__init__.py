# -*- coding: utf-8 -*-
"""
DataMining Techniques

Outliers detection methods
Data exploratory plots


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

from . import data
from . import explore
from . import outliers


