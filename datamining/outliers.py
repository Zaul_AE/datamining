# -*- coding: utf-8 -*-
"""
Outliers detection techniques

Outliers detection methods
    Median Absolute Deviation (MAD)
    Percentile based method (IQR)
    Zscore based method (ZSCORE)


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numba as nb

CLASS1 = type(pd.DataFrame())
CLASS2 = type(pd.Series())
CLASS3 = np.ndarray


# =============================================================================
# Outlier detection functions
# =============================================================================

def mad_outliers_detection(data, columns, threshold=3.5, group_by=None):
    """
    Median Absolute Deviation Outlier detection
    Returns a DataFrame of booleans with True if values are outliers

    INPUTS:
        data:        [DataFrame] input table of attributes
        columns:     [str, tuple, list] column or list of columns to search outliers
        threshold:   [int, float] The modified z-score to use as a threshold.
                      Observations with a modified z-score (based on the median
                      absolute deviation) greater than this value will be classified
                      as outliers.
        group_by:    [str] optional column to group data. When group_by is not None,
                      outliers per attribute are search by groups.

    OUTPUTS:
        outliers:    [DataFrame] detected outliers per column


    Reference:
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """

    if type(data) != CLASS1:
        raise TypeError('data parameter must be a DataFrame')
    if type(columns) not in (str, list, tuple):
        raise TypeError('columns parameter must be a string or tuple/list of strings')
    if type(group_by) is not str and group_by is not None:
        raise TypeError('group_by parameter must be a string')
    assert threshold > 0, 'bad threshold value'

    # Initial parameters
    if type(columns) is str:
        columns = (columns,)
    if group_by is None:
        group_values = []
    else:
        group_values = data[group_by].unique()
        data = data.sort_values(group_by)

    # Search outliers
    outliers = pd.DataFrame(np.zeros((data.shape[0], len(columns)), dtype=bool),
                            index=data.index,
                            columns=columns)
    for label in columns:
        if group_by is None:
            outliers.loc[:, label] = _mad_outlier(data.loc[:, label].values,
                                                  threshold)
        else:
            for value in group_values:
                rows = data.loc[:, group_by] == value
                outliers.loc[rows, label] = _mad_outlier(data.loc[rows, label].values,
                                                         threshold)

    return outliers


def iqr_outliers_detection(data, columns, factor=3, group_by=None):
    """
    Percentile based outliers detection using IQR
    Returns a DataFrame of booleans with True if values are outliers

    INPUTS:
        data:        [DataFrame] input table of attributes
        columns:     [str, tuple, list] column or list of columns to search outliers
        factor:      [int, float] inter quartile multiplier
        group_by:    [str] optional column to group data. When group_by is not None,
                      outliers per attribute are search by groups.

    OUTPUTS:
        outliers:    [DataFrame] detected outliers per column

    """

    if type(data) != CLASS1:
        raise TypeError('data parameter must be a DataFrame')
    if type(columns) not in (str, list, tuple):
        raise TypeError('columns parameter must be a string or tuple/list of strings')
    if type(group_by) is not str and group_by is not None:
        raise TypeError('group_by parameter must be a string')
    assert factor > 0, 'bad threshold value'

    # Initial parameters
    if type(columns) is str:
        columns = (columns,)
    if group_by is None:
        group_values = []
    else:
        group_values = data[group_by].unique()
        data = data.sort_values(group_by)

    # Search outliers
    outliers = pd.DataFrame(np.zeros((data.shape[0], len(columns)), dtype=bool),
                            index=data.index,
                            columns=columns)
    for label in columns:
        if group_by is None:
            outliers.loc[:, label] = _iqr_outlier(data.loc[:, label].values,
                                                  factor)
        else:
            for value in group_values:
                rows = data.loc[:, group_by] == value
                outliers.loc[rows, label] = _iqr_outlier(data.loc[rows, label].values,
                                                         factor)

    return outliers


def zscore_outliers_detection(data, columns, factor=1.96, group_by=None):
    """
    Standard deviation outliers detection using Z-Score
    Returns a DataFrame of booleans with True if values are outliers

    INPUTS:
        data:        [DataFrame] input table of attributes
        columns:     [str, tuple, list] column or list of columns to search outliers
        factor:      [int, float] standard deviation multiplier
        group_by:    [str] optional column to group data. When group_by is not None,
                      outliers per attribute are search by groups.

    OUTPUTS:
        outliers:    [DataFrame] detected outliers per column

    """

    if type(data) != CLASS1:
        raise TypeError('data parameter must be a DataFrame')
    if type(columns) not in (str, list, tuple):
        raise TypeError('columns parameter must be a string or tuple/list of strings')
    if type(group_by) is not str and group_by is not None:
        raise TypeError('group_by parameter must be a string')
    assert factor > 0, 'bad threshold value'

    # Initial parameters
    if type(columns) is str:
        columns = (columns,)
    if group_by is None:
        group_values = []
    else:
        group_values = data[group_by].unique()
        data = data.sort_values(group_by)

    # Search outliers
    outliers = pd.DataFrame(np.zeros((data.shape[0], len(columns)), dtype=bool),
                            index=data.index,
                            columns=columns)
    for label in columns:
        if group_by is None:
            outliers.loc[:, label] = _zscore_outlier(data.loc[:, label].values,
                                                     factor)
        else:
            for value in group_values:
                rows = data.loc[:, group_by] == value
                outliers.loc[rows, label] = _zscore_outlier(data.loc[rows, label].values,
                                                            factor)

    return outliers


# =============================================================================
# Plot outliers functions
# =============================================================================

def plot_outliers(serie, outliers, label=None, limits=None, kind='violin',
                  color='Greys', orient='v', **kargs):
    """
    Plot True data and outliers
    True data is can be ploted as violin, box, boxen and outliers as marks

    INPUTS:
        serie:       [Serie] input data serie
        outliers:    [DataFrame] outliers DataFrame. Columns names are used
                      as the method name. Indexes must be equal than the input serie
        label:       [str] optional variable name. If None, label is extracted
                      from input serie
        limits:      [tuple, list] min and max values to plot
        kind:        [str] kinf of plot ('violin', 'box', 'boxen')
        color:       [str] palette color
        orient:      [str] orientation plot ('h', 'v')
        kargs:       additional inputs ('ax', 'fontsize', 'marker', 'markercolor')

    OUTPUTS:
        fig, ax:     matplotlib objects (figure, axis)

    """

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)
    marker = kargs.get('marker', '.')
    markercolor = kargs.get('markercolor', 'k')
    if 'ax' in kargs:
        ax = kargs['ax']
        fig = ax.figure
    else:
        fig, ax = plt.subplots(figsize=figsize)

    if label is None:
        label = serie.name

    # Plot True data
    data = serie.to_frame()
    data.columns = ['data']
    data['mask'] = 'Original'

    for col in outliers:
        new_data = serie[~outliers[col]].to_frame()
        new_data['mask'] = col
        new_data.columns = ['data', 'mask']

        data = pd.concat((data, new_data))

    if orient.lower() == 'v':
        if kind.lower() == 'violin':
            sns.violinplot(x='mask', y='data', data=data, palette=color,
                           inner="quart", ax=ax)
        elif kind.lower() == 'box':
            sns.boxplot(x='mask', y='data', data=data, palette=color, ax=ax)
        elif kind.lower() == 'boxen':
            sns.boxenplot(x='mask', y='data', data=data, palette=color, ax=ax)
        ax.set_xlabel('')
        ax.set_ylabel(label, fontsize=fontsize)

    else:
        if kind.lower() == 'violin':
            sns.violinplot(y='mask', x='data', data=data, palette=color,
                           inner="quart", ax=ax)
        elif kind.lower() == 'box':
            sns.boxplot(y='mask', x='data', data=data, palette=color, ax=ax)
        elif kind.lower() == 'boxen':
            sns.boxenplot(y='mask', x='data', data=data, palette=color, ax=ax)
        ax.set_ylabel('')
        ax.set_xlabel(label, fontsize=fontsize)

    # Plot outliers
    for i, col in enumerate(outliers):
        xval = np.ones(outliers[col].sum()) + i
        yval = serie[outliers[col]]
        if orient.lower() == 'v':
            ax.scatter(xval, yval, marker=marker, color=markercolor)
        else:
            ax.scatter(yval, xval, marker=marker, color=markercolor)

    # Axis properties
    sns.despine()
    if limits:
        if orient.lower() == 'v':
            ax.set_ylim(limits)
        else:
            ax.set_xlim(limits)

    return fig, ax


# =============================================================================
# Low level functions
# =============================================================================

@nb.jit(nopython=True)
def _mad_outlier(data, threshold):
    median = np.median(data)
    diff = np.abs(data - median)
    med_abs_deviation = np.median(diff)
    modified_z_score = 0.6745 * diff / med_abs_deviation
    return modified_z_score > threshold


@nb.jit(nopython=True)
def _iqr_outlier(data, factor):
    x25 = np.percentile(data, 25)
    x75 = np.percentile(data, 75)
    iqr = x75 - x25
    return (data < x25 - factor * iqr) | (data > x75 + factor * iqr)


@nb.jit(nopython=True)
def _zscore_outlier(data, factor):
    std = np.std(data)
    mean = np.mean(data)
    diff = np.abs(data - mean)
    return diff > factor * std
