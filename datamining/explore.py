# -*- coding: utf-8 -*-
"""
Explore and visualize data


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats as spstats


# =============================================================================
# Explore multiple variables
# =============================================================================

def correlation_matrix(data, upper='scatter', lower='kde', kde=False, size=4,
                       palette='Blues_d', color='b', **kargs):
    """
    Plot a correlation matrix using different plot kinds in lower and upper triangles

    plots:    'scatter', 'reg', 'kde', 'hist'
    """

    marker = kargs.get('marker', '.')
    linecolor = kargs.get('linecolor', 'k')
    linestyle = kargs.get('linestyle', '-')
    linewidth = kargs.get('linewidth', 1)
    fitorder = kargs.get('fitorder', 1)
    alpha = kargs.get('alpha', 1)
    bins = kargs.get('bins', 30)

    upper = upper.lower()
    lower = lower.lower()

    def corrfunc(x, y, **kws):
        r, _ = spstats.pearsonr(x, y)
        ax = plt.gca()
        ax.annotate("r = {:.2f}".format(r),
                    xy=(.1, .9), xycoords=ax.transAxes)

    def pairgrid_hist(x, y, **kws):
        cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
        plt.hist2d(x, y, cmap=palette, cmin=1, **kws)

    g = sns.PairGrid(data, height=size, despine=True)
    g.map_diag(sns.distplot, kde=kde, color=color, hist_kws={'alpha':alpha})

    if upper == 'kde':
        g.map_upper(sns.kdeplot, cmap=palette, alpha=alpha)
    elif upper == 'hist':
        g.map_upper(pairgrid_hist, bins=bins)
    elif upper == 'reg':
        g.map_upper(sns.regplot, marker=marker, color=color, order=fitorder, ci=None,
                    line_kws={'color': linecolor,'linewidth': linewidth, 'linestyle':linestyle},
                    scatter_kws={'alpha': alpha})
    else:
        g.map_upper(plt.scatter, marker=marker, color=color, alpha=alpha)
        g.map_upper(corrfunc)
    if lower == 'kde':
        g.map_lower(sns.kdeplot, cmap=palette, alpha=alpha)
    elif lower == 'hist':
        g.map_lower(pairgrid_hist, bins=bins)
    elif lower == 'reg':
        g.map_lower(sns.regplot, marker=marker, color=color, order=fitorder, ci=None,
                    line_kws={'color': linecolor,'linewidth': linewidth, 'linestyle':linestyle},
                    scatter_kws={'alpha': alpha})
    else:
        g.map_lower(plt.scatter, marker=marker, color=color, alpha=alpha)
        g.map_lower(corrfunc)

    plt.show()
    plt.tight_layout()

    return g


def plot_multiple_dist(data, kind='violin', rows=None, cols=None,
                       color='Blues', **kargs):
    """
    Plots a violinplot or boxplot for each variable in DataFrame.
    This tools allows to plot variables with different scales
    """

    if type(data) != type(pd.DataFrame()):
        raise TypeError('data must be a Pandas DataFrame!')

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)

    n = data.shape[1]
    labels = data.columns

    if rows is None and cols is None:
        cols = n
    if rows is None:
        rows = 1
    if cols is None:
        cols = 1

    fig = plt.figure(figsize=figsize)

    g = np.empty((rows, cols), dtype=object)
    cnt = -1
    for i in range(rows):
        for j in range(cols):
            cnt += 1
            if cnt == n:
                break

            ax = plt.subplot(rows, cols, cnt+1)
            if kind.lower() == 'violin':
                sns.violinplot(y=data.loc[:, labels[cnt]].dropna(),
                               palette=color, inner="quart", ax=ax)
            elif kind.lower() == 'box':
                sns.boxplot(y=data.loc[:, labels[cnt]].dropna(),
                            palette=color, ax=ax)
            else:
                sns.boxenplot(y=data.loc[:, labels[cnt]].dropna(),
                              palette=color, ax=ax)

            ax.set_ylabel('')
            ax.set_title(labels[cnt], fontsize=fontsize)
            sns.despine(bottom=True)

            g[i, j] = ax

    plt.tight_layout()
    plt.show()

    return fig, g


def plot_multiple_hist(data, rows=None, cols=None, kde=False,
                       hist=True, color='b', **kargs):
    """Plots histograms for each column in a DataFrame"""

    if type(data) != type(pd.DataFrame()):
        raise TypeError('data must be a Pandas DataFrame!')

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)

    n = data.shape[1]
    labels = data.columns

    if rows is None and cols is None:
        cols = n
    if rows is None:
        rows = 1
    if cols is None:
        cols = 1

    fig = plt.figure(figsize=figsize)
    g = np.empty((rows, cols), dtype=object)
    cnt = -1
    for i in range(rows):
        for j in range(cols):
            cnt += 1
            if cnt == n:
                break

            ax = plt.subplot(rows, cols, cnt+1)
            sns.distplot(data.loc[:, labels[cnt]].dropna(), hist=hist,
                         kde=kde, color=color, ax=ax)
            ax.set_ylabel('')
            ax.set_xlabel('')
            ax.set_title(labels[cnt], fontsize=fontsize)
            sns.despine()

            g[i, j] = ax

    plt.tight_layout()
    plt.show()

    return fig, g


# =============================================================================
# Multiple axis
# =============================================================================

def plot_two_axis(y1, y2, x=None, left_ylabel=None, right_ylabel=None,
                        color=None, ax=None, **kargs):
    """Plot multiple data in two y axis"""

    if type(y1) != type(pd.DataFrame()):
        raise TypeError('y1 must be a Pandas DataFrame!')
    if type(y2) != type(pd.DataFrame()):
        raise TypeError('y2 must be a Pandas DataFrame!')

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)
    style_left = kargs.get('style_left', '-')
    style_rigth = kargs.get('style_left', '--')

    if left_ylabel is None:
        left_ylabel = 'Left'
    if right_ylabel is None:
        right_ylabel = 'Right'

    # Init plot
    if ax is None:
        fig, ax1 = plt.subplots(figsize=figsize)
    else:
        fig = ax.figure
        ax1 = ax

    # Plot data
    y1.plot(colormap=color, style=style_left, ax=ax1)
    y2.plot(secondary_y=list(y2.columns), style=style_rigth,
            colormap=color, ax=ax1)

    ax2 = ax1.right_ax
    ax1.set_xlabel('')
    ax1.set_ylabel(left_ylabel, fontsize=fontsize)
    ax2.set_ylabel(right_ylabel, fontsize=fontsize)
    plt.tight_layout()

    ax = (ax1, ax2)
    return fig, ax


def plot_diststat(data, x, y, kind='violin', stat='mean',
                  color=None, ax=None, **kargs):
    """Plot the data distribution and a statistic as line over the same scale"""

    if type(data) != type(pd.DataFrame()):
        raise TypeError('data must be a Pandas DataFrame!')

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)
    linecolor = kargs.get('linecolor', 'k')
    linestyle = kargs.get('linestyle', '-')
    linewidth = kargs.get('linewidth', 1)
    ylabel = kargs.get('ylabel', y)

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig = ax.figure

    # Plot distributions
    if kind.lower() == 'violin':
        ax = sns.violinplot(x=x, y=y, data=data, palette=color,
                            inner="quart", ax=ax)
    elif kind.lower() == 'box':
        ax = sns.boxplot(x=x, y=y, data=data, palette=color, ax=ax)
    else:
        ax = sns.boxenplot(x=x, y=y, data=data, palette=color, ax=ax)

    # Plot line by statistic
    if stat.lower() == 'mean':
        y1 = data[[x, y]].groupby(x).mean()
    elif stat.lower() == 'min':
        y1 = data[[x, y]].groupby(x).min()
    elif stat.lower() == 'max':
        y1 = data[[x, y]].groupby(x).max()
    y1['x_label'] = np.arange(0, y1.shape[0])

    y1.plot(x='x_label', y=y, ax=ax, linewidth=linewidth,
            linestyle=linestyle, color=linecolor, legend=False)

    # Set axes properties
    ax.set_ylabel(ylabel, fontsize=fontsize)
    ax.set_xlabel('')
    plt.tight_layout()

    return fig, ax


def plot_distline(data, x, y1, y2, kind='violin', stat='count', color=None,
                  ax=None, left_ylabel=None, right_ylabel=None, **kargs):
    """Plot the data distribution at left axis and a line in right axis with different scale"""

    if type(data) != type(pd.DataFrame()):
        raise TypeError('data must be a Pandas DataFrame!')

    figsize = kargs.get('figsize', None)
    fontsize = kargs.get('fontsize', 12)
    linecolor = kargs.get('linecolor', 'k')
    linestyle = kargs.get('linestyle', '-')
    linewidth = kargs.get('linewidth', 1)

    if left_ylabel is None:
        left_ylabel = y1
    if right_ylabel is None:
        right_ylabel = y2

    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    else:
        fig = ax.figure

    # Plot distributions
    if kind.lower() == 'violin':
        ax = sns.violinplot(x=x, y=y1, data=data, palette=color,
                            inner="quart", ax=ax)
    elif kind.lower() == 'box':
        ax = sns.boxplot(x=x, y=y1, data=data, palette=color, ax=ax)
    else:
        ax = sns.boxenplot(x=x, y=y1, data=data, palette=color, ax=ax)

    # Plot line by statistic
    if stat.lower() == 'count':
        yd2 = data[[x, y2]].groupby(x).count()
    elif stat.lower() == 'mean':
        yd2 = data[[x, y2]].groupby(x).mean()
    elif stat.lower() == 'min':
        yd2 = data[[x, y2]].groupby(x).min()
    elif stat.lower() == 'max':
        yd2 = data[[x, y2]].groupby(x).max()
    elif stat.lower() == 'sum':
        yd2 = data[[x, y2]].groupby(x).sum()
    yd2['x_label'] = np.arange(0, yd2.shape[0])

    yd2.plot(x='x_label', secondary_y=y2, ax=ax, linewidth=linewidth,
             linestyle=linestyle, color=linecolor, legend=False)

    # Set axes properties
    ax.set_ylabel(left_ylabel, fontsize=fontsize)
    ax.set_xlabel('')

    ax2 = ax.right_ax
    ax2.set_xlabel('')
    ax2.set_ylabel(right_ylabel, fontsize=fontsize)

    plt.tight_layout()

    return fig, (ax, ax2)


# =============================================================================
# Data conversion functions
# =============================================================================

def convertcol2num(data, columns=None, dtype='float', noval=None, inplace=False):
    """Convert string columns to numerical values"""

    if type(data) != type(pd.DataFrame()):
        raise TypeError('data must be a Pandas DataFrame!')
    if columns is not None and type(columns) not in (tuple, list):
        raise TypeError('columns must be a tuple or list of strings')
    if type(dtype) not in (str, tuple, list):
        raise TypeError('dtype must be a string or a tuple/list of strings')

    if columns is None:
        columns = list(data.columns)
    if type(dtype) is str:
        dtype = [dtype] * len(columns)
    if len(dtype) != len(columns):
        raise ValueError('lenght of columns and dtype must be equal')
    if noval is None:
        noval = np.nan

    def convert_float(x):
        try:
            return float(x)
        except ValueError:
            return noval

    def convert_int(x):
        try:
            return int(x)
        except ValueError:
            return noval


    convert_func = {
        'float': convert_float,
        'int': convert_int,
    }

    slice = data.loc[:, columns]

    for i in range(len(columns)):
        slice.iloc[:, i] = slice.iloc[:, i].apply(
                                               convert_func.get(dtype[i].lower()),
                                               noval)
    if inplace:
        data.loc[:, columns] = slice
    else:
        return slice
