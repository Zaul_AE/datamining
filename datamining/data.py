# -*- coding: utf-8 -*-
"""
Fix and convert data


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
import pandas as pd


# =============================================================================
# Working with DataFrames
# =============================================================================

def melt_dataframes(df1, df2, dropna=True):
    """
    Melts two DataFrames using the common column names

    :param df1:     [DataFrame] input DataFrame
    :param df2:     [DataFrame] input DataFrame
    :param dropna:  [bool] remove pair of data if any nan
    :return:        [DataFrame] melted DataFrame
    """

    cols = df1.columns
    data1, data2, index, labels = [], [], [], []
    for col in cols:
        if col in df1 and col in df2:
            data = pd.concat((df1.loc[:, col], df2.loc[:, col]), axis=1)
            if dropna:
                data = data.dropna()
            lab = np.full(data.shape[0], col, dtype=object)

            data1.extend(data.iloc[:, 0])
            data2.extend(data.iloc[:, 1])
            index.extend(data.index.values)
            labels.extend(lab)

    return pd.DataFrame({'value1': data1, 'value2': data2, 'name': labels, 'index': index})



