# DataMining
> A set of exploratory, visualization, outliers detection, and classification techniques for data analysis.


## Installation

OS X, Linux & Windows:

```sh
git clone https://gitlab.com/Zaul_AE/datamining.git
cd datamining
python setup.py install
```

## Dependencies

Following Python modules are required to install **datamining**:

* numpy
* scipy
* pandas
* matplotlib
* seaborn


## Usage example

A few examples about the models are shown in Example folder.

```python
import datamining as dm
```

## Release History
* 0.0.1
    * Work in progress

## Meta

Saul Arciniega Esparza – zaul.ae@gmail.com

[Twitter](https://twitter.com/zaul_arciniega) -
[Hashnode](https://hashnode.com/@ZaulAE) -
[ResearchGate](https://www.researchgate.net/profile/Saul_Arciniega-Esparza)


Distributed under the GNU V3 license. See ``LICENSE`` for more information.

[https://gitlab.com/Zaul_AE/datamining](https://gitlab.com/Zaul_AE/datamining)
