# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()

setup(
    name='dataminig',
    version='0.0.1',
    author='Saul Arciniega Esparza',
    description='Data Mining techniques',
    license='GNU3',
    packages=find_packages(exclude=['build', 'dist', '*.egg-info']),
    classifiers=[
    'Development Status :: DataMining algorithms',
    'Intended Audience :: Engineering software and DataScience',
    'Programming Language :: Python',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Programming Language :: Python :: 3.6, 3.7',
    ],
    install_requires=requirements
)
